extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var curHp : int = 10
var maxHp : int = 10
var moveSpeed : int = 250
var damage : int = 1
 
var gold : int = 0
 
var curLevel : int = 0
var curXp : int = 0
var xpToNextLevel : int = 50
var xpToLevelIncreaseRate : float = 1.2
 
var interactDist : int = 70
 
var vel = Vector2()
var facingDir = Vector2()
 
onready var rayCast = $RayCast2D
onready var anim = $AnimatedSprite
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	vel = Vector2()
	if Input.is_action_pressed("ui_up"):
		vel.y -= 1
		facingDir = Vector2(0, -1)
	if Input.is_action_pressed("ui_down"):
		vel.y += 1
		facingDir = Vector2(0,1)
	if Input.is_action_pressed("ui_left"):
		vel.x -= 1
		facingDir = Vector2(-1, 0)
	if Input.is_action_pressed("ui_right"):
		vel.x += 1
		facingDir = Vector2(1, 0)
	vel = vel.normalized()
	move_and_slide(vel * moveSpeed, Vector2.ZERO)
	manage_animations()

func play_animation (anim_name):
 
	if anim.animation != anim_name:
		anim.play(anim_name)

func manage_animations():
	if vel.x > 0:
		play_animation("Walk_Right")
	elif vel.x < 0:
		play_animation("Walk_Left")
	elif vel.y < 0:
		play_animation("Walk_Back")
	elif vel.y > 0:
		play_animation("Walk_Front")
	elif facingDir.x == 1:
		play_animation("Idle_Right")
	elif facingDir.x == -1:
		play_animation("Idle_Left")
	elif facingDir.y == -1:
		play_animation("Idle_Back")
	elif facingDir.y == 1:
		play_animation("Idle_Front")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
